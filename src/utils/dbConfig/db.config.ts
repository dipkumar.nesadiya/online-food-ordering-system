import { Sequelize } from 'sequelize';
import 'dotenv/config';

const DATABASE_NAME: string = process.env.DATABASE_NAME || '';
const DATABASE_USERNAME: string = process.env.DATABASE_USERNAME || '';
const DATABASE_PASSWORD: string = process.env.DATABASE_PASSWORD || '';

const sequelize = new Sequelize(
    DATABASE_NAME,
    DATABASE_USERNAME,
    DATABASE_PASSWORD,
    {
        dialect: 'mysql',
        host: 'localhost',
    }
);

export default sequelize;